def fibonacci(n):
    a, b = 1, 1
    for i in range (1, n):
        a, b = b, a + b
    return b

if __name__ == "__main__":
    print(fibonacci(0))

