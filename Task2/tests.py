from main import fibonacci
import Test

Test.assert_equals(fibonacci(3), 3)
Test.assert_equals(fibonacci(7), 21)
Test.assert_equals(fibonacci(12), 233)
Test.assert_equals(fibonacci(0), 1)
Test.assert_equals(fibonacci(1), 1)