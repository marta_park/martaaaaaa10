def oddish_or_evenish(num):
    sumofnum = 0

    stringnum = str(num)   


    for n in stringnum:
        sumofnum += int(n)

    if sumofnum % 2 == 0:
        return "Evenish"
    else:
        return "Oddish"

if __name__ == "__main__":
    print(oddish_or_evenish(11))

