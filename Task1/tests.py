from main import oddish_or_evenish
import Test

Test.assert_equals(oddish_or_evenish(43), "Oddish")
Test.assert_equals(oddish_or_evenish(373), "Oddish")
Test.assert_equals(oddish_or_evenish(55551), "Oddish")
Test.assert_equals(oddish_or_evenish(694), "Oddish")
Test.assert_equals(oddish_or_evenish(4433), "Evenish")
Test.assert_equals(oddish_or_evenish(11), "Evenish")
Test.assert_equals(oddish_or_evenish(211112), "Evenish")