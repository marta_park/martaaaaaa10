class MinStack:
    def __init__(self):
        self.stack = []

    def push(self, val):
        self.stack.append(int(val))

    def pop(self):
        self.stack.pop()
        
    def top(self):
        return self.stack[len(self.stack) - 1]
    
    def getMin(self):
        min = int(self.stack[0])
        for i in range(0, len(self.stack)):
            if (min > int(self.stack[i])):
                min = int(self.stack[i])
        return min

    def len(self):
        return len(self.stack)

def parse(commands, values):
    stack = MinStack()
  
    if (commands[0] != "MinStack") or (len(commands) != len(values)):
        return []
    ans = []
    ans.append(None)

    for i in range(1, len(commands)):

        if (commands[i] == "getMin"):
            ans.append(stack.getMin())
        elif (commands[i] == "push"):
            stack.push(values[i][0])
            ans.append(None)
        elif (commands[i] == "pop"):
            stack.pop()
            ans.append(None)
        elif (commands[i] == "top"):
            ans.append(stack.top())

    return ans