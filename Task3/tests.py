import test
from marta import parse

test.assert_equals(parse(["MinStack","push","push","push","getMin","pop","top","getMin"], [[],[-2],[0],[-3],[],[],[],[]]), [None,None,None,None,-3,None,0,-2])
test.assert_equals(parse(["MinStack","push","push","push","push","push","getMin","pop","pop","top","getMin"], [[],[1],[-2],[3],[-4],[5],[],[],[],[],[]]), [None,None,None,None,None,None,-4,None,None,5,-2])
