def assert_equals(ansFunct, ans):
    print(ansFunct)
    print(ans)

    if (len(ansFunct) != len(ans)):
        print(False) 
        return
    
    for i in range(0, len(ans)):
        if (ansFunct[i] != ans[i]):
            print(False)
            return

    print(True)